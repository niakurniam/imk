import SimpleOpenNI.*;
import java.util.*;

SimpleOpenNI context;
color[] userClr = new color[]
{
  color(255, 0, 0),
  color(0, 255, 0),
  color(0, 0, 255),
  color(255, 255, 0),
  color(255, 0, 255),
  color(0, 255, 255)
};
PVector com = new PVector();
PVector com2d = new PVector();
ArrayList<PVector> track_tangan_kanan = new ArrayList<PVector>();
ArrayList<PVector> track_tangan_kiri = new ArrayList<PVector>();
ArrayList<Integer> sign_track_tangan_kanan = new ArrayList<Integer>();
Integer sign = 0;

PImage gambar_eraser_kanan, gambar_pensil_kanan, gambar_tangan_kanan, gambar_tangan_kiri;
PImage menu_eraser, menu_refresh, menu_save, menu_writing;
PImage menu_false_eraser, menu_false_refresh, menu_false_save, menu_false_writing, menu_logo;
boolean eraser = false, refresh = true, save = true, writing = false;
int icon_size = 120;
PVector coord_menu_writing = new PVector(50, 290);
PVector coord_menu_eraser = new PVector(50, 430);
PVector coord_menu_refresh = new PVector(50, 150);
PVector coord_menu_save = new PVector(50, 570);
//PVector coord_menu_logo = new PVector(0,0);
PVector sekarang = new PVector();
int timer = millis();
PImage capture;

void setup()
{
  size(displayWidth, displayHeight, P2D);
  if (frame != null) frame.setResizable(true);

  context = new SimpleOpenNI(this);
  if (context.isInit() == false)
  {
    println("Can't init SimpleOpenNI, maybe the camera is not connected!");
    exit();
    return;
  }

  context.enableDepth();
  context.enableUser();

  reset();

  //stroke(0, 0, 255);
  //strokeWeight(3);
  smooth();
  
  gambar_eraser_kanan = loadImage("img/eraser_kanan.png");
  gambar_pensil_kanan = loadImage("img/pensil_kanan.png");
  gambar_tangan_kanan = loadImage("img/tangan_kanan.png");
  gambar_tangan_kiri = loadImage("img/tangan_kiri.png");
  menu_eraser = loadImage("img/eraser.png");
  menu_refresh = loadImage("img/refresh.png");
  menu_save = loadImage("img/save.png");
  menu_writing = loadImage("img/writing.png");
  menu_logo = loadImage("img/logo.png");
  menu_false_eraser = loadImage("img/eraser.png");
  menu_false_refresh = loadImage("img/refresh.png");
  menu_false_save = loadImage("img/save.png");
  menu_false_writing = loadImage("img/writing.png");
  menu_false_eraser.filter(GRAY);
  menu_false_refresh.filter(GRAY);
  menu_false_save.filter(GRAY);
  menu_false_writing.filter(GRAY);
  menu_eraser.resize(icon_size, icon_size);
  menu_refresh.resize(icon_size, icon_size);
  menu_save.resize(icon_size, icon_size);
  menu_writing.resize(icon_size, icon_size);
  menu_false_eraser.resize(icon_size, icon_size);
  menu_false_refresh.resize(icon_size, icon_size);
  menu_false_save.resize(icon_size, icon_size);
  menu_false_writing.resize(icon_size, icon_size);
}

void draw()
{
  context.update();

  reset();

  noStroke();
  fill(162, 205, 182);
  rect(0, 0, 220, displayHeight);

  //stroke(0, 0, 0);
  noStroke();
  fill(255, 255, 255);
  rect(220 + 10, 10, displayWidth - 240, displayHeight - 90);
  
  image(menu_logo, 0, 0); //nge-load logo

  if (writing == true) image(menu_writing, coord_menu_writing.x, coord_menu_writing.y);
  else image(menu_false_writing, coord_menu_writing.x, coord_menu_writing.y);

  if (eraser == true) image(menu_eraser, coord_menu_eraser.x, coord_menu_eraser.y);
  else image(menu_false_eraser, coord_menu_eraser.x, coord_menu_eraser.y);

  if (refresh == true) image(menu_refresh, coord_menu_refresh.x, coord_menu_refresh.y);
  else image(menu_false_refresh, coord_menu_refresh.x, coord_menu_refresh.y);

  if (save == true) image(menu_save, coord_menu_save.x, coord_menu_save.y);
  else image(menu_false_save, coord_menu_save.x, coord_menu_save.y);

  gambar();
  
  int[] userList = context.getUsers();
  for (int i = 0; i < userList.length; i++)
  {
    if (context.isTrackingSkeleton(userList[i]))
    {
      stroke(userClr[(userList[i] - 1) % userClr.length]);
      drawSkeleton(userList[i]);
    }
  }

  if (millis() - timer > 1500)
  {
    if (save == true) save = false;
    if (refresh == true) refresh = false;
     
    if (sekarang.x > coord_menu_writing.x && sekarang.x < coord_menu_writing.x + icon_size
      && sekarang.y > coord_menu_writing.y && sekarang.y < coord_menu_writing.y + icon_size)
      {
        writing = !writing;
        if (writing == true) sign = 1;
        else sign = 0;
        if (eraser == true) eraser = false;
        if (refresh == true) refresh = false;
        if (save == true) save = false;
        timer = millis();
      }
    if (sekarang.x > coord_menu_eraser.x && sekarang.x < coord_menu_eraser.x + icon_size
      && sekarang.y > coord_menu_eraser.y && sekarang.y < coord_menu_eraser.y + icon_size)
      {
        eraser = !eraser;
        if (eraser == true) sign = 2;
        else sign = 0;
        if (writing == true) writing = false;
        if (refresh == true) refresh = false;
        if (save == true) save = false;
        timer = millis();
      }
    if (sekarang.x > coord_menu_refresh.x && sekarang.x < coord_menu_refresh.x + icon_size
      && sekarang.y > coord_menu_refresh.y && sekarang.y < coord_menu_refresh.y + icon_size)
      {
        refresh = !refresh;
        if (refresh == true)
        {
          track_tangan_kanan.clear();
          sign_track_tangan_kanan.clear();
          sign = 0;
        }
        if (eraser == true) eraser = false;
        if (writing == true) writing = false;
        if (save == true) save = false;
        timer = millis();
      }
    if (sekarang.x > coord_menu_save.x && sekarang.x < coord_menu_save.x + icon_size
      && sekarang.y > coord_menu_save.y && sekarang.y < coord_menu_save.y + icon_size)
      {
        save = !save;
        if (save == true)
        {
          noStroke();
          fill(255, 255, 255);
          rect(220 + 10, 10, displayWidth - 240, displayHeight - 90);
          gambar();
          capture = get(220 + 10, 10, displayWidth - 240, displayHeight - 90);
          int d = day();
          int h = hour();
          int m = minute();
          int s = second();
          String hari = String.valueOf(d);
          String jam = String.valueOf(h);
          String menit = String.valueOf(m);
          String detik = String.valueOf(s);
          capture.save("Gambar" + hari + "-" + jam + "_" + menit + "_" + detik + ".jpg");
        }
        if (eraser == true) eraser = false;
        if (refresh == true) refresh = false;
        if (writing == true) writing = false;
        timer = millis();
      }
  }
}

void drawSkeleton(int userId)
{
  PVector tangan_kanan = new PVector();
  context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HAND, tangan_kanan);
  context.convertRealWorldToProjective(tangan_kanan, tangan_kanan);
  tangan_kanan.x = tangan_kanan.x * displayWidth / 640;
  tangan_kanan.y = tangan_kanan.y * (displayHeight + 360) / 480;
  track_tangan_kanan.add(tangan_kanan);
  sign_track_tangan_kanan.add(sign);
  if (writing == true) image(gambar_pensil_kanan, tangan_kanan.x % displayWidth, tangan_kanan.y % displayHeight);
  else if (eraser == true) image(gambar_eraser_kanan, tangan_kanan.x % displayWidth, tangan_kanan.y % displayHeight);
  else image(gambar_tangan_kanan, tangan_kanan.x % displayWidth, tangan_kanan.y % displayHeight);

  PVector tangan_kiri = new PVector();
  context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HAND, tangan_kiri);
  context.convertRealWorldToProjective(tangan_kiri, tangan_kiri);
  tangan_kiri.x = tangan_kiri.x * displayWidth / 640;
  tangan_kiri.y = tangan_kiri.y * (displayHeight + 360) / 480;
  track_tangan_kiri.add(tangan_kiri);
  image(gambar_tangan_kiri, tangan_kiri.x % displayWidth, tangan_kiri.y % displayHeight);
  sekarang = track_tangan_kiri.get(track_tangan_kiri.size() - 1);
}

// -----------------------------------------------------------------
// SimpleOpenNI events

void onNewUser(SimpleOpenNI curContext, int userId) {
  println("onNewUser - userId: " + userId);
  println("\tstart tracking skeleton");

  curContext.startTrackingSkeleton(userId);
}

void onLostUser(SimpleOpenNI curContext, int userId)
{
  println("onLostUser - userId: " + userId);
}

void onVisibleUser(SimpleOpenNI curContext, int userId)
{
}

void keyPressed()
{
  switch (key)
  {
    case ' ':
    context.setMirror(!context.mirror());
    break;
  }
}

void reset()
{
  background(247, 235, 176);
}

boolean sketchFullScreen()
{
  return true;
}

void gambar()
{
 int r = 255;
 boolean gradasi_warna = false;
 for (int i = 0; i < track_tangan_kanan.size() - 1; i++)
  {
    if (track_tangan_kanan.get(i).x > 220 + 10 && track_tangan_kanan.get(i).x < displayWidth - 240 + 220 + 10
    && track_tangan_kanan.get(i).y > 10 && track_tangan_kanan.get(i).y < displayHeight - 90 + 10
    && track_tangan_kanan.get(i + 1).x > 220 + 10 && track_tangan_kanan.get(i + 1).x < displayWidth - 240 + 220 + 10
    && track_tangan_kanan.get(i + 1).y > 10 && track_tangan_kanan.get(i + 1).y < displayHeight - 90 + 10)
    {
      if (sign_track_tangan_kanan.get(i) == 1)
      {
        //stroke(0);
        if (r > 255) gradasi_warna = true;
        if (r < 0) gradasi_warna = false;
        
        stroke (r, 1, 10);
        strokeWeight(5);
        line(track_tangan_kanan.get(i).x, track_tangan_kanan.get(i).y, track_tangan_kanan.get(i + 1).x, track_tangan_kanan.get(i + 1).y);
        
        if (gradasi_warna == true) r--;
        else r++;
      }
      else if (sign_track_tangan_kanan.get(i) == 2)
      {
        stroke(255);
        strokeWeight(50);
        line(track_tangan_kanan.get(i).x, track_tangan_kanan.get(i).y, track_tangan_kanan.get(i + 1).x, track_tangan_kanan.get(i + 1).y);
      }
    }
  }
}
